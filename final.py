from __future__ import print_function
import re

class Stack:
    def __init__(self):
        self.items = []

    def isEmpty(self):
        return self.items == []

    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def peek(self):
        return self.items[len(self.items)-1]

    def size(self):
        return len(self.items)

class BinaryTree:
    def __init__(self,rootObj):
        self.key = rootObj
        #self.parent = None
        self.leftChild = None
        self.rightChild = None

    def insertLeft(self,newNode):
        if isinstance(newNode, BinaryTree):
            t = newNode
        else:
            t = BinaryTree(newNode)

        if self.leftChild is not None:
            t.left = self.leftChild

        self.leftChild = t

    def insertRight(self,newNode):
        if isinstance(newNode,BinaryTree):
            t = newNode
        else:
            t = BinaryTree(newNode)

        if self.rightChild is not None:
            t.right = self.rightChild
            
        self.rightChild = t
        
    def delete(self):
        if self.leftChild is not None and self.rightChild is None:
            self = self.leftChild
            return self

    def isLeaf(self):
        return ((not self.leftChild) and (not self.rightChild))

    def getRightChild(self):
        return self.rightChild

    def getLeftChild(self):
        return self.leftChild

    def setRootVal(self,obj):
        self.key = obj

    def getRootVal(self,):
        return self.key

    def inorder(self):
        if self.leftChild:
            self.leftChild.inorder()
        print(self.key)
        if self.rightChild:
            self.rightChild.inorder()

    def postorder(self):
        if self.leftChild:
            self.leftChild.postorder()
        if self.rightChild:
            self.rightChild.postorder()
        print(self.key)

    def preorder(self):
        print(self.key)
        if self.leftChild:
            self.leftChild.preorder()
        if self.rightChild:
            self.rightChild.preorder()

    def printexp(self):
        if self.leftChild:
            print('(', end=' ')
            self.leftChild.printexp()
        print(self.key, end=' ')
        if self.rightChild:
            self.rightChild.printexp()
            print(')', end=' ')

    def postordereval(self):
        opers = {'+':operator.add, '-':operator.sub, '*':operator.mul, '/':operator.truediv}
        res1 = None
        res2 = None
        if self.leftChild:
            res1 = self.leftChild.postordereval()  #// \label{peleft}
        if self.rightChild:
            res2 = self.rightChild.postordereval() #// \label{peright}
        if res1 and res2:
            return opers[self.key](res1,res2) #// \label{peeval}
        else:
            return self.key

#proposition = p0 | p1 | .. | pn
#bin_con = and | or
#rule = proposition | rule bin_con rule 

def simple_check(flist, position, length):
    while position < length:
        if flist[position - 1] != ']':
            return 0
        position = position + 1
    return 1

def form_tree(expression):
    flist = expression.split()
    #print(flist)
    pStack = Stack()  
    eTree = BinaryTree('')
    pStack.push(eTree)
    currentTree = eTree
    length = len(flist)
    if length < 3:
        print("The expression is not valid")
    elif length == 3:
        currentTree.setRootVal(flist[1])
        return eTree
    else:
        position = 1
        previous = "None"
        lookahead = flist[position]
        for i in flist:
            if i == '[':
                currentTree.insertLeft('')
                pStack.push(currentTree)
                currentTree = currentTree.getLeftChild()
            elif i not in ['and', 'or', ']']:
                if previous not in ['and', 'or']:
                    currentTree.setRootVal(str(i))
                    parent = pStack.pop()
                    currentTree = parent
                else:
                    if lookahead != ']':
                        #print(previous + " " + i)
                        currentTree.insertLeft('')
                        pStack.push(currentTree)
                        currentTree = currentTree.getLeftChild()
                        currentTree.setRootVal(str(i))
                        parent = pStack.pop()
                        currentTree = parent
                    else:
                        currentTree.setRootVal(str(i))
                        parent = pStack.pop()
                        currentTree = parent
            elif i in ['and', 'or']:
                currentTree.setRootVal(i)
                currentTree.insertRight('')
                pStack.push(currentTree)
                currentTree = currentTree.getRightChild()
            elif i == ']':
                val = currentTree.getRootVal()
                expHasFinished = simple_check(flist, position, length)
                check = False
                while (val == 'and' or val == 'or') and position < length and expHasFinished == 0 and not pStack.isEmpty():
                    #print(val + " " + currentTree.getLeftChild().getRootVal() + " " + str(position) + " " + str(i))
                    parent = pStack.pop()
                    currentTree = parent
                    val = currentTree.getRootVal()
                    check = True
                if expHasFinished == 0 and pStack.isEmpty():
                    node = BinaryTree('')
                    node.leftChild = currentTree
                    currentTree = node
                    pStack.push(currentTree)
                    eTree = currentTree
                if expHasFinished == 1:
                    position = length
                    if val == '' and check is True:
                        eTree = currentTree.delete()
                    break
            else:
                raise ValueError
            previous = i
            position = position + 1
            if position < length:
                lookahead = flist[position]
    return eTree

# [ False or [ False or False ] or [ True and False ] ]  
# [ [ [ [ False or [ False or True ] and True ] or False or False ] and True ]      
# [ False or False or False ]
#"[ [ True and True and True ] or False or False or False or False ]"
#"[ [ [ [ False or [ False or True ] and True ] or False or False ] and True ]"
#"[ [ True or False ] and [ False or True or True or False or False or True or True or False ] ]"
# "[ False or False or False or [ False and False ] or True or True or False or False or True or False or False or False or False or False or False or [ True and False and False and False ] ]"

#error 
# [ False or [ False and True ] or False or False or False or False ]
#"[ False or [ False and False ] or True or False or False or False ]"
#"[ False or False or False or [ False and False ] or True or True or False or False or True or False or False or False or False or False or False or [ True and False and False and False ] ]"

#pt = form_tree("[ [ False and False ] or False or False or False or True or False ]")
#pt.printexp()

def evaluate(parseTree):
    leftC = parseTree.getLeftChild()
    rightC = parseTree.getRightChild()

    if leftC and rightC:
        fn = parseTree.getRootVal()
        if fn == 'and':
            return (evaluate(leftC) and evaluate(rightC))
        elif fn == 'or':
            return (evaluate(leftC) or evaluate(rightC))
    else:
        return eval(parseTree.getRootVal())
        
#print('\nIt evaluates to: ' + str(evaluate(pt)) + '\n')

def label_transactions():
    #open the transactions file 
    text_file = open("query_results.txt", "r")
    #open the file where we write the labels
    write_file = open("query_labels.txt","w+")
    classified = 0
    numOfTrans = 20000
    for x in range(0,numOfTrans):
        rule = text_file.readline()
        label = apply_transactions_rules(rule)
        if label not in "UNCLASSIFIED\n":
            classified += 1
        result = str(rule) + 10 * " " + str(label)
        write_file.write(result + '\n')
    #calculate the percentage of classified transactions
    cls_percentage = float(classified) / float(numOfTrans) * 100
    #calculate the percentage of unclassfied transactions
    uncls_percentage = 100 - cls_percentage 
    print("CLASSIFIED TRANSACTIONS: " + str(cls_percentage) + "%")
    print("UNCLASSIFIED TRANSACTIONS: " + str(uncls_percentage) + "%")
    #close the transactions file
    text_file.close()
    #close the file where we write the labels
    write_file.close()

def apply_transactions_rules(transaction):
    #define the label as UNCLASSIFIED 
    label = "UNCLASSIFIED\n"
    result = read_rules(transaction)
    if result is not None:
        label = result
    return label

def isKeyInTrans(string, transaction, hasNot):
    if hasNot == 0:
        if string in transaction:
            return True
    else:
        if string not in transaction:
            return True
    return False     
    
def read_rules(transaction):
    #open the rules file
    text_file = open("rules_final.txt", "r")
    for x in range (0,45):
        potential_label = text_file.readline()
        rule = text_file.readline()
        #skip the space line
        space_line = text_file.readline()
        #parse the line that contains the rule
        result = parse_line(rule, transaction)
        if result is True:
            return potential_label
    #close the rules file
    text_file.close()
    return None
    
def parse_line(line, transaction):
    #print(str(line))
    #extract the keywords from the transaction description
    keywords = re.findall(r'\((.*?)\)', line)
    #line[line.findall("(")+1:line.findall(")")] 
    #print("The keywords are:" + "\n" + str(keywords) + '\n')
    
    con = []
    nexus = ['and', 'or', 'not', '[', ']']
    words = re.compile('[a-zA-Z0-9_\[\]]+').findall(line)
    #extract the binary connectives and the nots from the transaction description
    for string in words:
        s = string
        if s in nexus:
            con.append(s)
    #print("The cons are:" + "\n" + str(con) + "\n") 
    
    x = 0
    case = 0
    position = 0
    command = "[ "
    length = len(con)
    for y in range(0,len(keywords)):
        #print(command)
        #set the lookahead keyword
        if position + 1 < len(keywords):
            lookahead = keywords[position + 1]
        else:
            lookahead = None
        
        #skip the brackets 
        while x < length and con[x] in ['[', ']']:
            command += con[x] + " "
            x = x + 1
        
        if case == 1 and x < length:
            command += con[x] + " "
            x = x + 1
            case = 0
        
        #skip the brackets 
        while x < length and con[x] in ['[', ']']:
            command += con[x] + " "
            x = x + 1
            
        if x < length:
            #print(str(x) + " " + str(length))
            if con[x] in ['and', 'or']:
                if x + 1 < length:
                    if con[x+1] not in [']']:
                        #str(keywords[position])
                        command += (str(isKeyInTrans(keywords[position], transaction, 0)) + " " + con[x] + " ")
                        x = x + 1
                    else:
                        command += (str(isKeyInTrans(keywords[position], transaction, 0)) + " " + con[x] + " " + str(isKeyInTrans(lookahead, transaction, 0)) + " ")
                        x = x + 1
                        position = position + 1
                        case = 1
                else:
                    command += (str(isKeyInTrans(keywords[position], transaction, 0)) + " " + con[x] + " ")
                    x = x + 1
            elif con[x] in ['not']:
                if x + 1 < length:
                    #con[x] + " " + str(keywords[position])
                    command += (str(isKeyInTrans(keywords[position], transaction, 1)) + " ")
                else:
                    #(con[x] + " " + str(keywords[position]))
                    command += (str(isKeyInTrans(keywords[position], transaction, 1)))
                x = x + 1
                if x < length:
                    if con[x] in [']']:
                        case = 1
                    elif con[x] in ['and', 'or']:
                        command += con[x] + " "
                        x = x + 1
                        if con[x] in [']']:
                            case = 1
                            position = position + 1
                            command += str(isKeyInTrans(keywords[position], transaction, 0)) + " "
        else:
            if length == 0 or con[x-1] not in [']']:
                command += str(isKeyInTrans(keywords[position], transaction, 0))
            break
        position = position + 1
        while position == len(keywords) and x < length and con[x] in [']']:
            command += con[x] + " "
            x = x + 1           
      
    if length > 0 and con[x-1] in [']']:  
        command += "]"
    else:
        command += " ]"
    #print("The command is:" + "\n" + command + "\n")
    
    pt = form_tree(command)
    result = evaluate(pt)
    #print('\nIt evaluates to: ' + str(result) + '\n')
    return result
    
#apply_transactions_rules("CARD CASHBACK PAYMENT TO SANTANDER CD PMT,260.18 GBP, RATE 1.00/GBPON 28-04-2015")
label_transactions()